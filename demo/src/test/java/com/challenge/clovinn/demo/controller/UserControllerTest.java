package com.challenge.clovinn.demo.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.testcontainers.containers.PostgreSQLContainer;

import com.challenge.clovinn.demo.dao.RoleRepository;
import com.challenge.clovinn.demo.exception.UnExpectedException;
import com.challenge.clovinn.demo.model.LocalUser;
import com.challenge.clovinn.demo.model.Role;
import com.challenge.clovinn.demo.service.ClientService;

import lombok.extern.slf4j.Slf4j;
import resources.CommonPostgresqlContainer;

@SpringBootTest
@Slf4j
public class UserControllerTest {
	@Autowired
	private UserController userController;

	@Autowired
	ClientService clientService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	BCryptPasswordEncoder encode;

	public static PostgreSQLContainer postgreSQLContainer = CommonPostgresqlContainer.getInstance();

	/**
	 * Intento agregar usuarios por la capa de seg, sin estar en sessión
	 */
	@Test // Sin seguridad
	void testAddUsers() {
		LocalUser user1 = creaUsuarios("Batman");
		LocalUser user2 = creaUsuarios("Robin");
		try {
			clientService.saveClient(user1);
			clientService.saveClient(user2);
		} catch (UnExpectedException e) {

		}

		assertEquals(2, clientService.findAll().size());

	}

	/**
	 * Pasando por controller / seguridad ON
	 */
	@Test
	void testAddUsersConSeguridad() {
		LocalUser user1 = creaUsuarios("Batman");
		try {
			userController.save(user1);
			assertEquals(1, userController.getUsuario(0));
		} catch (AuthenticationCredentialsNotFoundException ex) {
			assertTrue("Esto es un error esperado por no esta logueado", true);
		}

	}

	public LocalUser creaUsuarios(String nombre) {
		LocalUser user = new LocalUser();
		user.setEmail("tes@test.com");
		user.setEnabled(true);
		user.setName(nombre);
		user.setPassword("1234");

		Set<Role> set = new HashSet<Role>();
		set.add(crearRoles("USER"));
		user.setRoles(set);
		return user;
	}

	public Role crearRoles(String roleName) {
		Role role = new Role();
		role.setName(roleName);
		roleRepository.save(role);
		return role;
	}

	/**
	 * Test creación Role
	 */
	@Test
	public void validarRoles() {
		Role role = new Role();
		role.setName("ADMIN");
		roleRepository.save(role);
		role = new Role();
		role.setName("USER");
		role.setUsers(null);
		roleRepository.save(role);

		assertEquals(2, roleRepository.findAll().size());

	}

	/**
	 * Validar que la psw esta siendo
	 */
	@Test
	public void crearValidarPasswordEncoding() {
		try {
			clientService.saveClient(creaUsuarios("Superman"));
		} catch (UnExpectedException e) {
			assertFalse("Falló, no se logro almacenar el usaurio", true);
		}
		log.info("Comprobamos con el usuario SuperMan la psw");
		String psw = clientService.findById(1).getPassword();
		log.info("fin de la busquedam,  psw codificada: " + psw);

		String encodedPassword = encode.encode(psw);
		System.out.println();
		System.out.println("La password actual es         : " + psw);
		System.out.println("La encodificada es: " + encodedPassword);
		System.out.println();

		assertTrue(encode.matches(psw, encodedPassword));

	}

}
