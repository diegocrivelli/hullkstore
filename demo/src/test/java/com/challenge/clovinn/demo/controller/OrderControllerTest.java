package com.challenge.clovinn.demo.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;

import com.challenge.clovinn.demo.dao.BrandRepository;
import com.challenge.clovinn.demo.dao.RoleRepository;
import com.challenge.clovinn.demo.dao.UserRepository;
import com.challenge.clovinn.demo.exception.OrderWithoutProductsException;
import com.challenge.clovinn.demo.model.LocalUser;
import com.challenge.clovinn.demo.model.Order;
import com.challenge.clovinn.demo.model.Product;
import com.challenge.clovinn.demo.model.Role;
import com.challenge.clovinn.demo.model.Store;
import com.challenge.clovinn.demo.model.brands.AlternativeBrand;
import com.challenge.clovinn.demo.model.brands.Brand;
import com.challenge.clovinn.demo.model.brands.DCBrand;
import com.challenge.clovinn.demo.model.brands.MarvelBrand;
import com.challenge.clovinn.demo.model.payments.Cash;
import com.challenge.clovinn.demo.model.payments.IPayment;
import com.challenge.clovinn.demo.service.ProductService;
import com.challenge.clovinn.demo.service.StoreService;

import lombok.extern.slf4j.Slf4j;
import resources.CommonPostgresqlContainer;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@ActiveProfiles(value = "test")
@AutoConfigureMockMvc(addFilters = false)
@Slf4j
public class OrderControllerTest {
	@Autowired
	OrderController orderController;
	@Autowired
	ProductController productController;
	@Autowired
	StoreService storeService;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	BrandRepository brandRepository;

	@Autowired
	ProductService productService;
	@Autowired
	UserRepository userRepository;

	public static PostgreSQLContainer postgreSQLContainer = CommonPostgresqlContainer.getInstance();

	/**
	 * Validar cantidad esperada
	 */
	@Test
	public void listarTodas() {
		log.info("Se valida que es la primera vez, no hay ordenes. ");
		assertTrue(orderController.getAll().size() != 0);

	}

	/**
	 * Inizializa un store
	 */
	@Test
	public void generarStore() {
		Store hulkStore = new Store();
		hulkStore.setAddress("Calle sin numero 123");
		hulkStore.setEmail("test@test.com.ar");
		hulkStore.setName("Hulk Store");
		storeService.save(hulkStore);
		Store consultado = storeService.findById(1);
		log.info("Se encontro el Store con nombre: " + consultado.getName());
		assertFalse(consultado.getName().isEmpty());
	}

	/**
	 * Se intenta generar una orden sin elementos.
	 */
	@Test
	public void crearOrden() {
		Order order = new Order();
		order.setComment("Pedido solicitado desde el local.");

		order.setLocaluser(userRepository.findByUsername("UsuarioOrden1"));
		IPayment efectivo = new Cash();
		order.setPayment(efectivo.getDatail());
		log.info("Se procede a lanzar el guardaod de la orden, sin productos");
		try {

			ResponseEntity<Order> httpResponse = orderController.save(order);

		} catch (OrderWithoutProductsException ex) {
			log.info("Ocurrio un error esperado", ex.getMessage());
		}

	}

	/**
	 * Previo a guardar la orden, le agregamos y actualizamos el monto.
	 */
	@Test
	public void crearOrdenConProductos() {
		long total = 0;
		Order order = new Order();
		order.setComment("Pedido solicitado desde el local.");

		order.setLocaluser(userRepository.findByUsername("UsuarioOrden1"));
		IPayment efectivo = new Cash();
		order.setPayment(efectivo.getDatail());
		log.info("Se procede a lanzar el guardaod de la orden, CON productos");
		Product producto1 = productService.findById(50);
		Product producto2 = productService.findById(51);
		Product producto3 = productService.findById(52);
		List<Product> list = new ArrayList<Product>();
		list.add(producto1);
		list.add(producto2);
		list.add(producto3);
		for (Product product : list) {
			total += product.getPrice();
			product.setAmount(product.getAmount() - 1);
		}
		order.setTotal(total);
		order.setSelectedItems(list);

		ResponseEntity<Order> httpResponse = orderController.save(order);

		assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());
		log.info("Se recupero el siguinte estado de la llamada: " + httpResponse);

	}

	/**
	 * Creamos productos genericamente.
	 * 
	 * @param string
	 */

	private void crearProducto(String detalle, String alternativa) {
		Product producto = new Product();
		producto.setAmount(100);
		producto.setBrand(alternativa);
		producto.setDetail(detalle);
		producto.setName("Taza diseño");
		producto.setPrice(450);
		producto.setReserved(0);
		productController.save(producto);
		// return producto;

	}

	@Test
	public void crear5Productos() {
		int productosActuales = productController.getAll().size();
		for (int i = 0; i < 5; i++) {
			crearProducto("Producto numero" + i, "DC");
			crearProducto("Producto numero" + i, "MC");
			crearProducto("Producto numero" + i, "alternativo");
		}

		assertTrue((productosActuales + 15) == productController.getAll().size());

	}

	public Brand devolverMarca(String eleccion) {
		switch (eleccion) {
		case "MC":
			return brandRepository.getById(0);
		case "DC":
			return brandRepository.getById(1);
		default:
			return brandRepository.getById(2);

		}
	}

	public void creaUsuarios(String nombre) {
		LocalUser user = new LocalUser();
		user.setEmail("tes@test.com");
		user.setEnabled(true);
		user.setName(nombre);
		user.setPassword("1234");

		Set<Role> set = new HashSet<Role>();
		set.add(crearRoles("USER"));
		user.setRoles(set);
		userRepository.save(user);
	}

	public Role crearRoles(String roleName) {
		Role role = new Role();
		role.setName(roleName);
		roleRepository.save(role);
		return role;
	}

	@Test
	public void crearBrands() {
		log.info("Validamos de crear solamente una vez las marcas, para no deshabilitar");
		if (brandRepository.findAll().size() < 3) {
			brandRepository.save(new DCBrand());
			brandRepository.save(new MarvelBrand());
			brandRepository.save(new AlternativeBrand());
		}
		assertTrue(brandRepository.findAll().size() == 3);

	}

	@Test
	public void descontarStock() {
		log.info("Procedemos a evaluar el stock previo y post ejecución");
		long producto1 = productService.findById(50).getAmount();
		long producto2 = productService.findById(51).getAmount();
		long producto3 = productService.findById(52).getAmount();
		log.info("cantidad productos previo a ejecutar orden: " + "productos: " + producto1 + " -- " + producto2
				+ " -- " + producto3);
		crearOrdenConProductos();
		log.info("finalizo de ejecutar la orden");
		log.info("cantidad productos POST ejecutar --> : " + "productos: " + producto1 + " -- " + producto2
				+ " -- " + producto3);
		assertFalse("Evalumos los 3 Productos",
				(producto1 == productService.findById(50).getAmount() - 1)
						&& (producto2 == productService.findById(51).getAmount() - 1)
						&& (producto3 == productService.findById(52).getAmount() - 1));
		
		

	}

}
