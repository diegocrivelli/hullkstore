package com.challenge.clovinn.demo.exception;


public class OrderWithoutProductsException extends RuntimeException {

	private static final long serialVersionUID = 8800617648136712820L;

	public OrderWithoutProductsException(String message) {
		super(message);
	}

	public OrderWithoutProductsException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
	
}
