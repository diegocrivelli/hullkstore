package com.challenge.clovinn.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
@Entity
@Table(name = "PRODUCT")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id")
	private long id;
	@NotBlank
	@Column
	private String name;
	@Column
	private double price;
	@Column
	private String detail;
	@Column
	private int reserved;
	@Column
	private long amount;
	@Column
	private String brand;

	@ManyToOne
	@JoinColumn(name = "bach_id")
	private ProductBach bach;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private Order orderDetail;

}
