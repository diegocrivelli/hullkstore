package com.challenge.clovinn.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.clovinn.demo.service.ClientService;
import com.challenge.clovinn.demo.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;

/**
 * Controlador para las solicitudes de autenticación desde el cliente.
 * 
 * @author dcrivelli
 *
 */
@RestController
@RequestMapping("/autentication")
@Slf4j
@PreAuthorize("hasRole('ADMIN')")

public class AutenticationRequestController {

	@Autowired
	private UserService userService;

	@Autowired
	private ClientService clientService;

	@GetMapping("/{username}")
	@Operation(summary = "Busqueda de usuario por USERNAME, lo carga en el contexto de Spring Security")
	public ResponseEntity<HttpStatus> loadUserBySurname(@PathVariable("surname") String surname) {
		userService.loadUserByUsername(surname);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@GetMapping("/{id}")
	@Operation(summary = "Busqueda de usuario por ID, lo carga en el contexto de Spring Security")
	public ResponseEntity<HttpStatus> loadUserByID(@PathVariable("id") long id) {
		userService.loadUserByUsername(clientService.findById(id).getUsername());
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

}
