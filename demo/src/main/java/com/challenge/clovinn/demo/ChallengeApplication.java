package com.challenge.clovinn.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.extern.slf4j.Slf4j;

//@SpringBootApplication(exclude = { SecurityAutoConfiguration.class }) //deshabilito la seg
@SpringBootApplication
@Slf4j
@EnableJpaRepositories(basePackages = "com.challenge.clovinn.demo")
public class ChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
		log.info("Se inicia la applicación para Hulk Store!");
	}

}
