package com.challenge.clovinn.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.exception.OrderWithoutProductsException;
import com.challenge.clovinn.demo.model.Order;
import com.challenge.clovinn.demo.service.OrderService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/Order")
@Slf4j
@PreAuthorize("hasRole('USER')")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@GetMapping("/all")
	@Operation(summary = "Retorna todos las ordenes")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Order> getAll() {
		log.debug("Se solicita el listado de todos los productos disponibles");
		return orderService.findAll();

	}

	@PostMapping("/save")
	@Operation(summary = "Controller para manejo de nuevas ordenes")
	public ResponseEntity<Order> save(@RequestBody Order order) {
		if (order.getSelectedItems()==null) {
			throw new OrderWithoutProductsException("La orden ingresada debe contar con productos");
		}

		log.debug("Procedemos a guardar un nuevo producto: " + order);
		return new ResponseEntity<>(orderService.saveOrder(order), HttpStatus.CREATED);

	}

	@PutMapping("{id}")
	@Operation(summary = "Controller para manejo de las actualizaci�n en las ordenes/pedidos")
	public ResponseEntity<Order> update(@PathVariable long id, @RequestBody Order order) {
		log.info("Se procede a actualizar la orden con id: " + id);
		if (orderService.existById(id)) {
			return new ResponseEntity<>(orderService.updateOrder(order), HttpStatus.ACCEPTED);
		}

		throw new NotFoundException(
				"La orden con ID: " + id + " no fue encontrado para el usuario: " + order.getLocaluser().getName());

	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Controller para eliminar ordenes, es necesario el ID")
	public ResponseEntity<HttpStatus> deleteOrder(@PathVariable("id") long id) {
		log.debug("Procedemos a eliminar completamente la orden con ID: " + id);
		orderService.deleteOrder(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{id}")
	@Operation(summary = "Busqueda de ordenes por ID")
	public ResponseEntity<Order> getOrder(@PathVariable("id") long id) {
		return new ResponseEntity<>(orderService.findById(id), HttpStatus.OK);
	}

	@PostMapping("/reserve")
	@Operation(summary = "Controller reserva de productos, hasta que sean confirmados")
	public ResponseEntity<Order> reserve(@RequestBody Order order) {
		log.debug("Procedemos a reservar el pedido en el controller: " + order);
		return new ResponseEntity<>(orderService.reserve(order), HttpStatus.OK);
	}

}
