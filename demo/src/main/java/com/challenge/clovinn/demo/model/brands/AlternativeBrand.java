package com.challenge.clovinn.demo.model.brands;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class AlternativeBrand extends Brand {

	public AlternativeBrand() {
		super();
		this.setName("Alternative");
	}

}
