package com.challenge.clovinn.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.clovinn.demo.model.LocalUser;

@Repository
public interface UserRepository extends JpaRepository<LocalUser, Long> {

	LocalUser findByUsername(String username);

}
