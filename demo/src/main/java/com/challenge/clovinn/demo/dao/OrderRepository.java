package com.challenge.clovinn.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.clovinn.demo.model.Order;

/**
 * Repository gestionar los pedidos. 
 * 
 * @author dcrivelli
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

}