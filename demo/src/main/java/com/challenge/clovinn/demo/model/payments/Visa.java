package com.challenge.clovinn.demo.model.payments;

public class Visa implements IPayment {

	@Override
	public String getDatail() {
		return "VISA";
	}

	@Override
	public long getDiscount() {
		return 10;
	}

}
