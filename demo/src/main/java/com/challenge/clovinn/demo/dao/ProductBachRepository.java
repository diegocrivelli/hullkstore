package com.challenge.clovinn.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.clovinn.demo.model.ProductBach;

/**
 * Repository listados de productos por marca.
 * 
 * @author dcrivelli
 *
 */
@Repository
public interface ProductBachRepository extends JpaRepository<ProductBach, Long> {

}