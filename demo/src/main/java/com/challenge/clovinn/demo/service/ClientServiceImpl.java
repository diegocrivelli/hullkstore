package com.challenge.clovinn.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.challenge.clovinn.demo.dao.ClientRepository;
import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.exception.UnExpectedException;
import com.challenge.clovinn.demo.model.LocalUser;

import lombok.extern.slf4j.Slf4j;

/**
 * Capa de servicios para clientes.
 * 
 * @author dcrivelli
 */
@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepository;

	
	@Bean
	BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
//	@Autowired
//	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public List<LocalUser> findAll() {
		return clientRepository.findAll();
	}

	public LocalUser findById(long id) {
		return clientRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el clinte buscado por identificador"));
	}

	@Transactional
	public LocalUser saveClient(LocalUser client) throws UnExpectedException {
		try {
			client.setPassword(bCryptPasswordEncoder().encode(client.getPassword()));
			return clientRepository.save(client);
		} catch (Exception ext) {
			throw new UnExpectedException("No se logr� guardar el nuevo usuario: " + ext);
		}

	}

	public void deleteClient(long id) {
		clientRepository.deleteById(id);
	}

	public boolean existById(long id) {
		return clientRepository.existsById(id);
	}

	public boolean existByIdAndActive(long id) {
		log.debug("buscamos el usuario con ID: " + id);
		LocalUser client = clientRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el cliente buscado: " + id));
		if (client.getEnabled()) {
			return true;
		}

		return false;
	}

	public LocalUser updateClient(LocalUser clientUpdate) {
		log.debug("Se procede a actualizar el cliente: " + clientUpdate.getName());

		LocalUser client = clientRepository.findById(clientUpdate.getId()).orElseThrow(
				() -> new NotFoundException("No se encontro el cliente buscado: " + clientUpdate.getName()));
		if (clientUpdate.getName() != null) {
			client.setEmail(clientUpdate.getEmail());
			client.setEnabled(clientUpdate.getEnabled());
			client.setName(clientUpdate.getName());
			client.setOrders(clientUpdate.getOrders());
			client.setUsername(clientUpdate.getUsername());

		}
		return clientRepository.save(client);

	}

	public boolean updatePassword(String psw, long id) {
		log.debug("Se intentara actualizar la psw para el usuario con ID: " + id);
		LocalUser client = clientRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el cliente buscado: " + id));
		if (client.getEnabled() && client.hashCode() != -1) {
			client.setPassword(psw);
			return true;
		}
		return false;

	}

	@Override
	public void activate(boolean activate, long id) {
		log.debug("Activar usuario con el ID: " + id);
		LocalUser client = clientRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el cliente buscado: " + id));
		client.setEnabled(activate);
	}

	public LocalUser findByUsername(String username) {
		return clientRepository.findByUsername(username);
	}
	
}
