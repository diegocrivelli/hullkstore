package com.challenge.clovinn.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.clovinn.demo.model.Store;

/**
 * Repository para el Store.
 * 
 * @author dcrivelli
 *
 */
@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {

}