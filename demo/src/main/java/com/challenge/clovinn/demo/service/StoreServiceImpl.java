package com.challenge.clovinn.demo.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.clovinn.demo.dao.StoreRepository;
import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.model.ProductBach;
import com.challenge.clovinn.demo.model.Store;

import lombok.extern.slf4j.Slf4j;

/**
 * Capa de servicios para clientes.
 * 
 * @author dcrivelli
 */
@Slf4j
@Service
public class StoreServiceImpl implements StoreService {

	@Autowired
	private StoreRepository storeRepository;

	public Set<ProductBach> findAllProductBach() {
		return findById(0).getProductBachs();
	}

	public Store findById(long id) {
		return storeRepository.findById(id).orElseThrow(() -> new NotFoundException("No se encontro el store"));
	}

	public Store save(Store store) {
		return storeRepository.save(store);
	}

	public Store updateStore(Store storeUpdate) {
		log.debug("Se procede a actualizar el HulkStore");
		Store store = findById(storeUpdate.getId());
		if (store.getName() != null) {
			store.setAddress(storeUpdate.getAddress());
			store.setName(storeUpdate.getName());
			store.setEmail(store.getEmail());
			store.setProductBachs(storeUpdate.getProductBachs());

		}
		return storeRepository.save(store);

	}

}
