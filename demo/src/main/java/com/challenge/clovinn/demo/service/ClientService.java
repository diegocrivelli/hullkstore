package com.challenge.clovinn.demo.service;

import java.util.List;

import com.challenge.clovinn.demo.exception.UnExpectedException;
import com.challenge.clovinn.demo.model.LocalUser;

/**
 * Interfaz para la implementación de la capa de servicios correspondientes a
 * los clientes.
 * 
 * @author dcrivelli
 */
public interface ClientService {
	List<LocalUser> findAll();

	LocalUser findById(long id);

	LocalUser saveClient(LocalUser client) throws UnExpectedException;

	void deleteClient(long id);

	boolean existById(long id);
	
	boolean existByIdAndActive(long id);

	LocalUser updateClient(LocalUser client);
	
	boolean updatePassword(String psw, long id);
	
	void activate(boolean activate, long id);
	
	LocalUser findByUsername(String username);
}
