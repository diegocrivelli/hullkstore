package com.challenge.clovinn.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.clovinn.demo.model.brands.Brand;

/**
 * Repository gestionar las marcas 
 * 
 * @author dcrivelli
 *
 */
@Repository
public interface BrandRepository extends JpaRepository<Brand, Integer> {

}