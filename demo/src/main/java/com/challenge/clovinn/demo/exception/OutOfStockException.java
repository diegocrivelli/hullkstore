package com.challenge.clovinn.demo.exception;


public class OutOfStockException extends RuntimeException {

	private static final long serialVersionUID = 8800617648136712820L;

	public OutOfStockException(String message) {
		super(message);
	}

	public OutOfStockException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
	
}
