package com.challenge.clovinn.demo.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.challenge.clovinn.demo.dao.ProductBachRepository;
import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.model.ProductBach;

import lombok.extern.slf4j.Slf4j;

/**
 * Capa de servicios para los productos.
 * 
 * @author dcrivelli
 *
 */
/**
 * @author dcrivelli
 *
 */
/**
 * @author dcrivelli
 *
 */
@Slf4j
@Component
public class ProductBachServiceImpl implements ProductBachService {

	@Autowired
	private ProductBachRepository bachRepository;

	@Autowired
	private StoreService storeService;

	public List<ProductBach> findAll() {
		return bachRepository.findAll();
	}

	public ProductBach findById(long id) {
		return bachRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el producto buscado"));
	}

	public ProductBach saveBach(ProductBach bach) {
		return bachRepository.save(bach);
	}

	public void deleteBach(long id) {
		bachRepository.deleteById(id);
	}

	public boolean existById(long id) {
		return bachRepository.existsById(id);
	}

	@Override
	public ProductBach updateBach(ProductBach bachUpdate) {
		log.debug("Se procede a actualizar el listado de productos para: " + bachUpdate.getBrand().getName());

		ProductBach bach = bachRepository.findById(bachUpdate.getId())
				.orElseThrow(() -> new NotFoundException("No se encontro el producto buscado: " + bachUpdate.getId()));
		if (bachUpdate.getDescription() != null) {
			bach.setBrand(bachUpdate.getBrand());
			bach.setDescription(bachUpdate.getDescription());
			bach.setBachProducts(bachUpdate.getBachProducts());
		}
		return bachRepository.save(bach);

	}

	/**
	 * Buscamos la lista de producto por Marca que nos llega como par�metro.
	 */
	public ProductBach findByBrand(long brandID) {
		Set<ProductBach> bachs = storeService.findAllProductBach();
		return bachs.stream().filter(bach -> brandID == bach.getBrand().getId()).findFirst()
				.orElseThrow(() -> new NotFoundException("No se encontro la lista por marca"));

	}

}
