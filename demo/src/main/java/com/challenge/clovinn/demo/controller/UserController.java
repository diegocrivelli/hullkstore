package com.challenge.clovinn.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.exception.UnExpectedException;
import com.challenge.clovinn.demo.model.LocalUser;
import com.challenge.clovinn.demo.service.ClientService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
@PreAuthorize("hasRole('ADMIN')")
public class UserController {

	@Autowired
	private ClientService clientService;

	@GetMapping("/allUsers")
	@Operation(summary = "Retorna todos los usuarios almacenados")
	public ResponseEntity<List<LocalUser>> getAllusers() {
		log.debug("Se solicita el listado de todos los usuarios disponibles");
		return new ResponseEntity<>(clientService.findAll(), HttpStatus.OK);

	}

	@PostMapping("/save")
	@Operation(summary = "Controller ingresar nuevo usaurio")
	public ResponseEntity<LocalUser> save(@RequestBody LocalUser localUser) {
		log.debug("Procedemos a guardar un nuevo usuario: " + localUser);
		try {
			return new ResponseEntity<>(clientService.saveClient(localUser), HttpStatus.CREATED);
		} catch (UnExpectedException e) {
			e.printStackTrace();
		}
		return null;

	}

	@PutMapping("{id}")
	@Operation(summary = "Controller para actualizar usuarios por ID")
	public ResponseEntity<LocalUser> update(@PathVariable long id, @RequestBody LocalUser localUser) {
		log.info("Se procede a actualizar el usuariuo con id: " + id);
		if (clientService.existByIdAndActive(id)) {
			return new ResponseEntity<>(clientService.updateClient(localUser), HttpStatus.ACCEPTED);
		}

		throw new NotFoundException(
				"La el usaurio con ID: " + id + " no fue encontrado, usaurio: " + localUser.getName());
	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Controller para eliminar usuarios")
	public ResponseEntity<HttpStatus> deleteClient(@PathVariable("id") long id) {
		log.debug("Procedemos a eliminar completamente el  usuario con ID: " + id);
		clientService.deleteClient(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{id}")
	@Operation(summary = "Busqueda de usuario por ID")
	public ResponseEntity<LocalUser> getUsuario(@PathVariable("id") long id) {
		return new ResponseEntity<>(clientService.findById(id), HttpStatus.OK);
	}

	

}


