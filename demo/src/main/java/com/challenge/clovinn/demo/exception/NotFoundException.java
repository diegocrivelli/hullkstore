package com.challenge.clovinn.demo.exception;


public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8800617648136712820L;

	public NotFoundException(String message) {
		super(message);
	}

	public NotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
	
}
