package com.challenge.clovinn.demo.service;

import java.util.List;

import com.challenge.clovinn.demo.model.Order;

/**
 * Interfaz para la implementación de la capa de servicios para las ordenes.
 * 
 * @author dcrivelli
 */
public interface OrderService {

	List<Order> findAll();

	Order findById(long id);

	Order saveOrder(Order order);

	void deleteOrder(long id);

	boolean existById(long id);

	Order updateOrder(Order order);

	Order reserve(Order order);

}
