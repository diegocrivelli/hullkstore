package com.challenge.clovinn.demo.model.payments;

/**
 * Posibles formas de pagos, descuentos.
 * 
 * @author dcrivelli
 */
public interface IPayment {
	String getDatail();

	long getDiscount();
}
