package com.challenge.clovinn.demo.service;

import java.util.Set;

import com.challenge.clovinn.demo.model.ProductBach;
import com.challenge.clovinn.demo.model.Store;

/**
 * Interfaz para la implementaci�n para Store.
 * 
 * @author dcrivelli
 */
public interface StoreService {
	Set<ProductBach> findAllProductBach();

	Store save(Store store);

	Store updateStore(Store store);
	
	Store findById(long id);

}
