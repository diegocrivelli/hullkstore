package com.challenge.clovinn.demo.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.challenge.clovinn")
public class AppConfig {

	// beans para  configurar

}
