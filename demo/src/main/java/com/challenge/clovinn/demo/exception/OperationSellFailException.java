package com.challenge.clovinn.demo.exception;


public class OperationSellFailException extends RuntimeException {

	private static final long serialVersionUID = 8800617648136712820L;

	public OperationSellFailException(String message) {
		super(message);
	}

	public OperationSellFailException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
	
}
