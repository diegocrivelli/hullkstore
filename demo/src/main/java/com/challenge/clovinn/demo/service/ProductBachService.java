package com.challenge.clovinn.demo.service;

import java.util.List;

import com.challenge.clovinn.demo.model.ProductBach;

/**
 * Interfaz para la implementación de la capa para manejo de listado de
 * productos por marca.
 * 
 * @author dcrivelli
 */
public interface ProductBachService {
	List<ProductBach> findAll();

	ProductBach findById(long id);

	ProductBach saveBach(ProductBach bach);

	void deleteBach(long id);

	boolean existById(long id);

	ProductBach updateBach(ProductBach bach);
	
	ProductBach findByBrand(long brandID);

}
