package com.challenge.clovinn.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.challenge.clovinn.demo.dao.UserRepository;
import com.challenge.clovinn.demo.model.LocalUser;

/**
 * Servicio para cargar los usauarios en el contexto de seguridad
 * 
 * @todo Configurar al finalizar desarrollo/productivo.
 * 
 * @author dcrivelli
 */
@Service
public class UserService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LocalUser user = userRepository.findByUsername(username);
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ADMIN"));
		UserDetails userDetails = new User(user.getUsername(), user.getPassword(), authorities);

		return userDetails;

	}
}
