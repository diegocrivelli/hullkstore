package com.challenge.clovinn.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.challenge.clovinn.demo.model.Product;

/**
 * Repository de Productos.
 * 
 * @author dcrivelli
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}