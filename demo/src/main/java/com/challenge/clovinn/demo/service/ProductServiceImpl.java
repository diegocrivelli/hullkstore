package com.challenge.clovinn.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.clovinn.demo.dao.ProductRepository;
import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.model.Product;

import lombok.extern.slf4j.Slf4j;

/**
 * Capa de servicios para los productos.
 * 
 * @author dcrivelli
 *
 */
@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	public List<Product> findAll() {
		return productRepository.findAll();
	}

	public Product findById(long id) {
		return productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el producto buscado"));
	}

	@Transactional
	public Product saveProduct(Product product) {
		return productRepository.save(product);
	}

	public void deleteProduct(long id) {
		productRepository.deleteById(id);
	}

	public boolean existById(long id) {
		return productRepository.existsById(id);
	}

	@Override
	public Product updateProduct(Product productUpdate) {
		log.debug("Se procede a actualizar el producto: " + productUpdate);

		Product product = productRepository.findById(productUpdate.getId()).orElseThrow(
				() -> new NotFoundException("No se encontro el producto buscado: " + productUpdate.getId()));
		if (productUpdate.getName() != null) {
			product.setName(productUpdate.getName());
			product.setDetail(productUpdate.getDetail());
			product.setPrice(productUpdate.getPrice());
		}
		return productRepository.save(product);

	}

	public void increseProduct(long id) {
		log.debug("Se procede a imcrementar el stock del producto con ID: " + id);

		Product product = productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el producto buscado: " + id));
		product.setAmount(product.getAmount() + 1);

	}

	public void decreseProduct(long id) {
		log.debug("Se procede a decrementar el stock del producto con ID: " + id);

		Product product = productRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro el producto buscado: " + id));
		product.setAmount(product.getAmount() - 1);

	}

}
