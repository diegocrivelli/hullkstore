package com.challenge.clovinn.demo.exception;

public class UnExpectedException extends Exception {

	private static final long serialVersionUID = 8800617648136712820L;

	public UnExpectedException(String message) {
		super(message);
	}

	public UnExpectedException(String message, Throwable cause) {
		super(message, cause);
	}

}
