package com.challenge.clovinn.demo.service;

import java.util.List;

import com.challenge.clovinn.demo.model.Product;

/**
 * Interfaz para la implementación de la capa de servicios.
 * 
 * @author dcrivelli
 */
public interface ProductService {
	List<Product> findAll();

	Product findById(long id);

	Product saveProduct(Product product);

	void deleteProduct(long id);

	boolean existById(long id);

	Product updateProduct(Product product);

	void increseProduct(long id);

	void decreseProduct(long id);
}
