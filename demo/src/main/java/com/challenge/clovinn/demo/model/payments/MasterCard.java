package com.challenge.clovinn.demo.model.payments;

public class MasterCard implements IPayment {

	@Override
	public String getDatail() {
		return "Mastercard";
	}

	@Override
	public long getDiscount() {
		return 20;
	}

}
