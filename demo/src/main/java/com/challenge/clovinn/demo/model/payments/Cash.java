package com.challenge.clovinn.demo.model.payments;

public class Cash implements IPayment {

	@Override
	public String getDatail() {
		return "CASH";
	}

	@Override
	public long getDiscount() {
		return 0;
	}

}
