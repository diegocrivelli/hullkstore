package com.challenge.clovinn.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.clovinn.demo.dao.OrderRepository;
import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.exception.OperationSellFailException;
import com.challenge.clovinn.demo.exception.OutOfStockException;
import com.challenge.clovinn.demo.model.Order;
import com.challenge.clovinn.demo.model.Product;

import lombok.extern.slf4j.Slf4j;

/**
 * Capa de servicios para el manejo de las ordenes/pedidos.
 * 
 * @author dcrivelli
 *
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private ProductService productService;

	public List<Order> findAll() {
		return orderRepository.findAll();
	}

	public Order findById(long id) {
		return orderRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("No se encontro la orden buscada, deber�a generar una nueva"));
	}

	@Transactional
	public Order saveOrder(Order order) {
		log.debug("Se incia el proceso de generaci�n  de una nueva orden");
		order.getSelectedItems().stream().forEach((product) -> {
			try {
				/*
				 * ProductBach productBach = bachService.findByBrand(product.getBach().getId());
				 */
				Product selected = productService.findById(product.getId());
				// Si tengo Stock y los que estan siendo procesados no superan al total.
				if (selected.getAmount() > 0 && selected.getReserved() <= selected.getAmount()) {
					selected.setAmount(selected.getAmount() - 1);
					selected.setReserved(selected.getReserved() - 1);
				}

			} catch (OperationSellFailException operationFail) {
				log.error("No se logr� guardar el pedido");
				throw operationFail;
			}

		});

		return orderRepository.save(order);
	}

	public void deleteOrder(long id) {
		orderRepository.deleteById(id);
	}

	public boolean existById(long id) {
		return orderRepository.existsById(id);
	}

	public Order updateOrder(Order orderToUpdate) {
		log.debug("Se procede a actualizar el producto: " + orderToUpdate);

		Order order = orderRepository.findById(orderToUpdate.getId()).orElseThrow(
				() -> new NotFoundException("No se encontro el producto buscado: " + orderToUpdate.getId()));
		if (orderToUpdate.getLocaluser() != null) {
			order.setLocaluser(orderToUpdate.getLocaluser());
			order.setCreatedDate(orderToUpdate.getCreatedDate());
			order.setPayment(orderToUpdate.getPayment());
			order.setComment(orderToUpdate.getComment());

		}
		log.info("Se actualizo correctamente la orden para el cliente: " + order.getLocaluser().getName());
		return orderRepository.save(order);

	}

	/**
	 * realiza la reserva de los productos seleccionados en el front-end.
	 */
	public Order reserve(Order order) {
		order.getSelectedItems().stream().forEach((product) -> {
			log.debug("Se procede a reservar el producto: " + product);
			if (product.getReserved() < product.getAmount())
				product.setReserved(product.getReserved() + 1);
			else
				throw new OutOfStockException("No hay mas stock" + product.getDetail());
			;
		});
		log.info("Finalizo la reserva de productos para el orden: " + order.getId());
		return order;
	}

}
