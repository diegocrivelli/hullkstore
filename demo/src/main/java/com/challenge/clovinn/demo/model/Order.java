package com.challenge.clovinn.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ORDERS")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "order_id")
	private long id;

	@Column(name = "created_date")
	@CreatedDate
	private long createdDate;

	@Column
	String payment;

	@Column
	private String comment;
	
	@Column
	private double total;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private LocalUser localuser;

	@OneToMany(mappedBy = "orderDetail")
	private List<Product> selectedItems;

}
