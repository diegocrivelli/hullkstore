package com.challenge.clovinn.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.challenge.clovinn.demo.model.brands.Brand;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ProductBach {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bach_id")
	private long id;

	@NotBlank
	@NonNull
	@Column
	private String description;

	@Column(name = "created_date")
	@CreatedDate
	private long createdDate;

	@Column(name = "modified_date")
	@LastModifiedDate
	private long modifiedDate;
	
	@ManyToOne
	@JoinColumn(name = "brand_id")
	private Brand brand;	
	
	@OneToMany(mappedBy = "bach")
	private List<Product> bachProducts;

	@ManyToOne
	@JoinColumn(name = "store_id")
	private Store store;
	
}
