package com.challenge.clovinn.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.clovinn.demo.exception.NotFoundException;
import com.challenge.clovinn.demo.model.Product;
import com.challenge.clovinn.demo.service.ProductService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/product")
@Slf4j
@PreAuthorize("hasRole('USER')")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/all")
	@Operation(summary = "Retorna todos los productos almacenados")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Product> getAll() {
		log.debug("Se solicita el listado de todos los productos disponibles");
		return productService.findAll();

	}

	@PostMapping("/save")
	@Operation(summary = "Controller para manejo de inserci�n nuevos productos")
	public ResponseEntity<Product> save(@RequestBody Product product) {
		log.debug("Procedemos a guardar un nuevo producto: " + product);
		return new ResponseEntity<>(productService.saveProduct(product), HttpStatus.CREATED);

	}

	@PutMapping("{id}")
	@Operation(summary = "Controller para manejo de actualizaci�n nuevos productos")
	public ResponseEntity<Product> update(@PathVariable long id, @RequestBody Product product) {
		log.info("Se procede a actualizar el producto con id: " + id);
		if (productService.existById(id)) {
			return new ResponseEntity<>(productService.updateProduct(product), HttpStatus.ACCEPTED);
		}

		throw new NotFoundException("El producto ID" + id + " no fue encontrado");

	}

	@DeleteMapping("/{id}")
	@Operation(summary = "Controller para eliminar un producto en particular por ID")
	public ResponseEntity<HttpStatus> deleteProduct(@PathVariable("id") long id) {
		log.debug("Procedemos a borrar el producto con ID: " + id);
		productService.deleteProduct(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{id}")
	@Operation(summary = "Busqueda de un producto por ID")
	public ResponseEntity<Product> getProduct(@PathVariable("id") long id) {
		return new ResponseEntity<>(productService.findById(id), HttpStatus.OK);
	}

}
