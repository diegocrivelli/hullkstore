package com.challenge.clovinn.demo.model.brands;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class MarvelBrand extends Brand {

	public MarvelBrand() {
		super();
		this.setName("Marvel");
	}

}
