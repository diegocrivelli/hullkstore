# HullkStore

Proyecto DEMO para challenge Clovinn.
## Getting started
La finalidad de este proyecto es solo para test 


## Test and Deploy / Installation


Para levantar el contenedor ejecutar los pasos en el archivo, generar el build para docker: 
comandos-docker-build.txt (en el directorio root del proyecto):
- [ ] ```./gradlew clean build```
- [ ] ```docker build -t "demo-on-docker" .```
    

Para levantar postgres y su interfaz de administración levantar las imagenes docker con docker-compose: 
- [ ] ```docker-compose -f .\docker-compose-bd.yml```
Nota: para correr los test de unidad y revisar los logs, recomiendo esta primera opción. 


Para levantar la aplicación demo-on-docker y la BD el siguiente docker-compose: 
- [ ] ```docker-compose -f .\docker-compose-app-springboot.yml```


El builder debe correrse desde el directorio raíz. Ejecuta el archivo Dockerfile configurado para el proyecto springboot.

## Recursos para validar, testear la capa rest: 
- [ ] ```http://< IP del contenedor >:< port >/swagger-ui/index.html#/```


## repo/download

```
cd existing_repo
git remote add origin https://gitlab.com/diegocrivelli/hullkstore.git
git branch -M main
git push -uf origin main
```


## Name
Diego Crivelli

